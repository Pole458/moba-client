# MOBA Client #

This is a little MOBA made completely in Java. Here you can find the [server](https://bitbucket.org/Pole458/moba-server) and the [Map Creator](https://bitbucket.org/Pole458/moba-map-creator).

You can also find a video trailer here:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/KT5oj_REpf0/0.jpg)](http://www.youtube.com/watch?v=KT5oj_REpf0)

## The Game ##

This game can be played up to 6 people in a 3v3 mode. In order to win, each team has to carry a "ball" in the other's team base. The first team that scores two points wins the game.

## Champions ##

You can choose from 12 different champions:


**Nightdart** : DPS

![Image of Nightdart](sprites/Champions/Player1/Player1_0.png)

**Purprowl**: Assassin

![Image of Purprowl](sprites/Champions/Player2/Player2_0.png)

**Bullbot**: Tank

![Image of Bullbot](sprites/Champions/Player3/Player3_0.png)

**Hairwitch**: Mage

![Image of Hairwitch](sprites/Champions/Player4/Player4_0.png)

**Lord Agony**: Fighter

![Image of Lord Agony](sprites/Champions/Player5/Player5_0.png)

**Blurred**: Assassin

![Image of Blurred](sprites/Champions/Player6/Player6_0.png)

**Steamstoove**: Tank

![Image of Steamstoove](sprites/Champions/Player7/Player7_0.png)

**Swordensheeld**: Fighter

![Image of Swordensheeld](sprites/Champions/Player8/Player8_0.png)

**Onslaught**: Tank

![Image of Onslaught](sprites/Champions/Player9/Player9_0.png)

**Spike**: Fighter

![Image of Spike](sprites/Champions/Player10/Player10_0.png)

**Mastermnind**: Mage

![Image of Mastermind](sprites/Champions/Player11/Player11_0.png)

**Bullseye**: DPS

![Image of Bullseye](sprites/Champions/Player12/Player12_0.png)


## Controls ##

Movement | Auto Attack | Spell | Ultimate
---------|-------------|-------|---------
**wasd** | **K** | **J** | **L**


## Credits ##

Made by Pole.

Sprites ripped from the game "Lock's Quest", thanks to the guys on https://www.spriters-resource.com/
