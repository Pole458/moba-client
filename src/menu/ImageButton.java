package menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class ImageButton extends MyButton {
	private String image;
	
	public ImageButton(String name, String image, Color color, Rectangle size) {
		super(name,color,size);
		this.image=image;
	}
	
	public void draw(Graphics g) {
		g.setColor(color);
		if(selected) g.fillRect((int)size.getX(),(int)size.getY(),(int)size.getWidth(),(int)size.getHeight());
		else g.drawRect((int)size.getX(),(int)size.getY(),(int)size.getWidth(),(int)size.getHeight());
		g.drawImage(new ImageIcon(image).getImage(),(int)size.getCenterX()-(new ImageIcon(image)).getIconWidth()/2,(int)size.getCenterY()-(new ImageIcon(image)).getIconHeight()/2,null);
		g.setColor(Color.BLACK);
		g.drawString(name,(int)size.getCenterX()-name.length()*4,(int)size.getMaxY()-5);
	}
	
	public void setImage(String s) {
		image=s;
	}
}