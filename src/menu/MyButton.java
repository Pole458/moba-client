package menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

public class MyButton {
	protected Rectangle size;
	protected Color color;
	protected String name;
	protected boolean selected;

	public MyButton(String name) {
		this.name=name;
	}
	
	public MyButton(String name, Color color, Rectangle size) {
		this.size=size;
		this.color=color;
		this.name=name;
		this.selected=false;	
	}
	
	public void draw(Graphics g) {
		
		g.setColor(color);
		
		g.drawRect((int)size.getX(),(int)size.getY(),(int)size.getWidth()-1,(int)size.getHeight()-1);
		if(selected) g.fillRect((int)size.getX(),(int)size.getY(),(int)size.getWidth(),(int)size.getHeight());
		
		g.setFont(new Font("Look",Font.PLAIN,(int)(size.getHeight() - size.getHeight()/4)));
		g.setColor(Color.BLACK);
		ClientPage.drawStringCenter(g, name, (int)size.getCenterX(), (int)size.getCenterY());
	}
	
	
	//SETTERS*******************************************************************************************************
	public void setPosition(short x, short y) {
		size.setLocation(x,y);
	}
	public void setSize(short w, short h) {
		size.setSize(w, h);
	}
	public void setSize(Rectangle size) {
		this.size = size;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSelected(boolean s) {
		selected=s;
	}
	
	//GETTERS*******************************************************************************************************
	public Rectangle getSize() {
		return size;
	}
	public Color getColor() {
		return color;
	}
	public String getName() {
		return name;
	}
	public boolean isSelected() {
		return selected;
	}
}