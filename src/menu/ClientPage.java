package menu;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import client.Client;

@SuppressWarnings("serial")
public class ClientPage extends JPanel implements MouseListener, MouseMotionListener {
	
	public ClientPanel invitePanel;
	
	public ClientPage() {
		
		setLayout(null);
		
		addMouseListener(this);
		addMouseMotionListener(this);
		
		if(Client.invitePanel != null) {
			invitePanel = Client.invitePanel; 	//When creating a new page, he keeps the invite panel
			add(invitePanel);
		}
	}

	public static void drawStringCenter(Graphics g, String s, int x, int y) {
	
		g.drawString(s, x - g.getFontMetrics().stringWidth(s)/2, y + (g.getFontMetrics().getAscent() - g.getFontMetrics().getDescent() ) /2 );
	}
	
	@Override
	public void mouseDragged(MouseEvent arg0) {}

	@Override
	public void mouseMoved(MouseEvent arg0) {}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

}
