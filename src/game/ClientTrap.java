package game;

public class ClientTrap {
	private int x;
	private int y;
	private int tile;
	private boolean isActive;
	
	public ClientTrap(int x, int y, int t, boolean a) {
		this.x=x;
		this.y=y;
		tile=t;
		isActive=a;
	}
	
	public void setActive(boolean a) {
		isActive=a;
	}
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public boolean isActive() {
		return isActive;
	}
	public int getTile() {
		return tile;
	}
	
}