package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.swing.ImageIcon;
import menu.ClientPage;
import client.Client;

@SuppressWarnings("serial")
public class ClientGame extends ClientPage {

	private Thread output;
	
	private final int OUTPUTDELAY = 20;
	
	private int fps;
	private int fpsCount;
	private long sec;
	
	public static ClientMap map;
	private int inGameX;
	private int inGameY;
	
	private short blueScore;
	private short redScore;
	private boolean scorer;
	private short scoreAnimation;
	private long scoreMoment;
	
	private Unit[] units;
	private Projectile[] projs;
	
	public boolean drawing;
	
	private Unit[] receivedUnits;
	private Projectile[] receivedProj;
	
	private int clientNum;
	private int ping;
	
	public static boolean upKey;
	public static boolean downKey;
	public static boolean leftKey;
	public static boolean rightKey;
	public static boolean JKey;
	public static boolean KKey;
	public static boolean LKey;
	
	public int passiveCD;
	public boolean passiveColor;
	public int spellCD;
	public boolean spellColor;
	public int ultiCD;
	public boolean ultiColor;
	
	//Stats
	private short armor;
	private short damage;
	private short attTimer;
	private short lifeSteal;
	private short speed;
	
	//STARTING THE GAME***************************************************************************************************************************
	public ClientGame() {
		
		map = new ClientMap();
		inGameX = 0;
		inGameY = 0;

		drawing = true;
		
		scoreAnimation = 3000;
		scoreMoment = System.currentTimeMillis() - scoreAnimation - 1;
		
		upKey = false;
	    downKey = false;
	    leftKey = false;
	    rightKey = false;
	    
	    fpsCount = 0;
	    sec = System.currentTimeMillis();
	    
		try {
			
			readCreatedGatesData();
		
		} catch (IOException e) {
			Client.debugLog("Error! Could not read Created Game Data");
		}
		
		output = new Thread(new ServerO());
	    output.start();
	    
	    Client.debugLog("Client Game page created");
		
	}
	
	private void readCreatedGatesData() throws IOException {
		
		int x;
		int y;
		int tile;
		
		clientNum = Client.inDataStream.read();
		
		map.traps = new ClientTrap[Client.inDataStream.read()];
		
		for(int i=0;i<map.traps.length;i++) {
			x = Client.inDataStream.read();
			y = Client.inDataStream.read();
			tile = Client.inDataStream.read();
			map.traps[i] = new ClientTrap(x,y,tile,true);
	    }
		
		blueScore = (short) Client.inDataStream.read();
		redScore = (short) Client.inDataStream.read();
		
		readGates();
		
		readStats();
		
		readCD();
		
		readUnits();
		
		readProjectiles();

		drawing = false;
	
	}

	//STARTING THE GAME***************************************************************************************************************************
	//GRAPHICS***************************************************************************************************************************
	public synchronized void paint(Graphics g) {
		
		super.paint(g);
		
		if( !drawing ) {
    		try {
				wait();
    		} catch (InterruptedException e) {
				//Client.debugLog(e.getMessage());
			}
    	}
		
		drawing = true;
		
		Graphics2D g2d = (Graphics2D)g;
		
		follow(units[clientNum]);
    	
    	if(System.currentTimeMillis() < scoreMoment + scoreAnimation)
    		cameraToBase((short)(Math.max(0,50-(System.currentTimeMillis()-scoreMoment)*100/scoreAnimation)));
		
		drawMap(g2d);
		drawProjectiles(g2d);
		drawUnits(g2d);
		
		if(System.currentTimeMillis()<scoreMoment+scoreAnimation) {
			drawScore(g2d);
		}
		
		if(blueScore>1 || redScore>1) {
			 drawEnd(g2d);
		} else
			drawGUI(g2d);
		
		drawDebug(g2d);
		fpsCount++;
		
		Toolkit.getDefaultToolkit().sync();
        g.dispose();
        
        if(System.currentTimeMillis()-sec > 1000) {
        	sec = System.currentTimeMillis();
        	fps = fpsCount;
        	fpsCount = 0;
        }
        
        notify();
	}
	
	private void drawMap(Graphics2D g2d) {
		for(int l=0; l<3; l++)
			for(int i=inGameX/ClientMap.cellSize;i*ClientMap.cellSize<607+inGameX;i++)
				for(int j=inGameY/ClientMap.cellSize;j*ClientMap.cellSize<405+inGameY;j++) {
					if(map.cells[l][i][j]!=100)
						g2d.drawImage(map.drawCell(l,i,j),i*ClientMap.cellSize-inGameX,j*ClientMap.cellSize-inGameY,this);
			}
		for(int i=0; i<map.traps.length; i++) {
			if(map.traps[i].isActive()) g2d.drawImage(new ImageIcon("TileSet/tile_"+map.traps[i].getTile()+".png").getImage(),map.traps[i].getX()*ClientMap.cellSize-inGameX,map.traps[i].getY()*ClientMap.cellSize-inGameY,this);
		}
	}
	
	private void drawUnits(Graphics2D g2d) {
		Unit[] u = orderByDepth();
		
		for(int i=0; i<u.length;i++)
			if( u[i].getTeam() == units[clientNum].getTeam() || u[i].getVisible() )
				u[i].draw(g2d, inGameX, inGameY);
	}
	
	private void drawGUI(Graphics2D g2d) {
		
		for(int i=0; i<units.length;i++) {
			if( units[i].getTeam() == units[clientNum].getTeam() || units[i].getVisible() )
				units[i].drawHealthBar(g2d, inGameX, inGameY, this);
		}
		//drawDamage(g2d);
		drawHud(g2d, units[clientNum]);
	}
	
	private void drawHud(Graphics2D g2d, Unit u) {
		
		g2d.drawImage(new ImageIcon("sprites/GUI/HUD_1.png").getImage(), 0, 429-128,this);
		if(passiveColor) g2d.setColor(Color.GREEN);
		else g2d.setColor(Color.RED);
		g2d.drawRect(15, 429-127+78+18, 23, -24*passiveCD/100);
		if(spellColor) g2d.setColor(Color.GREEN);
		else g2d.setColor(Color.RED);
		g2d.drawRect(57, 429-127+78+18, 23, -24*spellCD/100);
		if(ultiColor) g2d.setColor(Color.GREEN);
		else g2d.setColor(Color.RED);
		g2d.drawRect(99, 429-127+78+18, 23, -24*ultiCD/100);
		g2d.drawImage(new ImageIcon("sprites/GUI/HUD_2.png").getImage(), 0, 429-127,this);
		g2d.setColor(Color.GREEN);
		g2d.fillRect(203, 429-127+77, Math.max(0, 202*u.getHP()/u.getHPMax()), 16);
		g2d.setColor(Color.BLACK);
		g2d.setFont(new Font("Look",Font.PLAIN,14));
		g2d.drawString(String.valueOf(units[clientNum].getHP() + "/" + units[clientNum].getHPMax()), 280, 429-127+85+4);
		
		g2d.setFont(new Font("Look",Font.PLAIN,10));
		g2d.setColor(Color.BLACK);
		
		g2d.drawString("Damage:", 12, 429-127+26);
		g2d.drawString(String.valueOf( damage ), 10+75, 429-127+26);
		
		g2d.drawString("Armor:", 12, 429-127+26+10);
		g2d.drawString(String.valueOf( armor ), 10+75, 429-127+26+10);
		
		g2d.drawString("Att.Spd:", 12, 429-127+26+20);
		g2d.drawString(String.valueOf((double)(100000/attTimer)/100), 10+75, 429-127+26+20);
		
		g2d.drawString("LifeSteal:", 12, 429-127+26+30);
		g2d.drawString(String.valueOf( lifeSteal ), 10+75, 429-127+26+30);
		
		g2d.drawString("Speed:", 12, 429-127+25+40);
		g2d.drawString(String.valueOf( speed ), 10+75, 429-127+26+40);
		
		g2d.setFont(new Font("Look",Font.PLAIN,12));
		g2d.setColor(Color.BLUE);
		g2d.drawString(String.valueOf(blueScore), 53, 429-127+14);
		g2d.setColor(Color.RED);
		g2d.drawString(String.valueOf(redScore), 78, 429-127+14);
		g2d.setColor(Color.BLACK);
	}
	
	private void drawProjectiles(Graphics2D g2d) {
		try{
			for(int i=0; i<projs.length;i++) {
				if(projs[i].getRange()==0)
					g2d.drawImage(new ImageIcon(projs[i].getSprite()).getImage(),projs[i].getX()-projs[i].getWidth()/2-inGameX,projs[i].getY()-projs[i].getHeight()/2-inGameY-15,this);
				else {
					if(projs[i].getTeam()=='B') g2d.setColor(Color.BLUE);
					if(projs[i].getTeam()=='R') g2d.setColor(Color.RED);
					if(projs[i].getTeam()=='N') g2d.setColor(Color.BLACK);
					
					g2d.drawOval(projs[i].getX()-projs[i].getRange()-inGameX,projs[i].getY()-projs[i].getRange()-inGameY, projs[i].getRange()*2, projs[i].getRange()*2);
					g2d.setColor(Color.BLACK);
				}
			}
		} catch(NullPointerException e) {
		}
	}
	
	private void drawDebug(Graphics2D g2d) {
		
		if(Client.debug) {
			g2d.setColor(Color.GREEN);
			g2d.drawString("FPS: "+String.valueOf(fps),400,50);
			g2d.drawString("Ping: "+String.valueOf(ping),400,75);
			g2d.drawString("X: "+ String.valueOf((int)units[clientNum].getX()) + ",Y: " + String.valueOf((int)units[clientNum].getY()),400,100);
			g2d.drawString(String.valueOf(passiveCD) + " " + String.valueOf(spellCD) + " " +String.valueOf(ultiCD), 400,125);
			g2d.drawString("Units: " + String.valueOf(units.length) + ", Projectiles:  " + String.valueOf(projs.length), 400,150);
			
			g2d.drawRect( units[clientNum].getX() - 10 - inGameX, units[clientNum].getY() - 10 - inGameY, 20, 20);
		}	
	}

	private void drawEnd(Graphics2D g2d) {
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_0.png").getImage(),0,200-63,null);
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_1.png").getImage(),0,(400-126)/2,null);
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_2.png").getImage(),0,(400-126)/2,null);
	
		g2d.setFont(new Font("Look",Font.PLAIN,14));
		
		if(blueScore>redScore) {
			g2d.setColor(Color.BLUE);
			g2d.drawString("VICTORY", 100, 200);
			g2d.setColor(Color.RED);
			g2d.drawString("DEFEAT", 450, 200);
		}
		else {
			g2d.setColor(Color.BLUE);
			g2d.drawString("DEFEAT", 100, 200);
			g2d.setColor(Color.RED);
			g2d.drawString("VICTORY", 450, 200);
		}
		g2d.setColor(Color.BLACK);
	}
	
	private void drawScore(Graphics2D g2d) {
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_0.png").getImage(),0,200-63,null);
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_1.png").getImage(),0,(400-126)/2,null);
		g2d.drawImage(new ImageIcon("sprites/GUI/end_game_2.png").getImage(),0,(400-126)/2,null);
	
		if(scorer) {
			g2d.setColor(Color.BLUE);
			g2d.drawString("Blue team scored!", 100, 200);
		}
		else {
			g2d.setColor(Color.RED);
			g2d.drawString("Red team scored!", 450, 200);
		}
		g2d.setColor(Color.BLACK);
	}
	
	private void cameraToBase(short perc) {
		if(scorer) {
			inGameX=((inGameX)*perc+(29*24-300)*(100-perc))/100;
			inGameY=((inGameY)*perc+(31*24-180)*(100-perc))/100;
		}
		else {
			inGameX=((inGameX)*perc+(12*24-300)*(100-perc))/100;
			inGameY=((inGameY)*perc+(14*24-180)*(100-perc))/100;
		}
		
		if(inGameX>ClientMap.tileX*ClientMap.cellSize-607) inGameX=ClientMap.tileX*ClientMap.cellSize-607;
		if(inGameY>ClientMap.tileY*ClientMap.cellSize-405) inGameY=ClientMap.tileY*ClientMap.cellSize-405;
		
		if(inGameX<0) inGameX=0;
		if(inGameY<0) inGameY=0;
		
	}
	
	private void follow(Unit u) {
		
		inGameX=(int)u.getX()-300;
		inGameY=(int)u.getY()-180;
		
		if(inGameX>ClientMap.tileX*ClientMap.cellSize-607) inGameX=ClientMap.tileX*ClientMap.cellSize-607;
		if(inGameY>ClientMap.tileY*ClientMap.cellSize-405) inGameY=ClientMap.tileY*ClientMap.cellSize-405;
		
		if(inGameX<0) inGameX=0;
		if(inGameY<0) inGameY=0;
	}
	
	public static int compareDepth(Unit u1, Unit u2) {
		return Float.compare(u1.getY(),u2.getY());
	}
	
	private Unit[] orderByDepth() {
		
		Unit[] tmp = new Unit[units.length];
		
		for(int i=0; i<tmp.length; i++) 
			tmp[i] = new Unit(units[i]);
		
		for(int i=1; i<tmp.length; i++) {
			for(int j=i-1; j>-1; j--) {
				if(compareDepth(tmp[j+1],tmp[j])<0) {
					Unit u = new Unit(tmp[j+1]);
					tmp[j+1] = tmp[j];
					tmp[j] = u;
				}
			}
		}
		return tmp;
	}
	//GRAPHICS***************************************************************************************************************************
	//OUTPUT TO SERVER***************************************************************************************************************************
	private class ServerO implements Runnable {

		@Override
		public void run() {
			
			Client.debugLog("Client key output Thread started");
			
			long beforeTime, timeDiff, sleep;
	        beforeTime = System.currentTimeMillis();
			
	        while(  blueScore<2 && redScore<2 ) {
				
				try {
					sendKeyStatus();
				} catch (IOException e1) {
				}

				timeDiff = System.currentTimeMillis() - beforeTime;
	            sleep = OUTPUTDELAY - timeDiff;

	            if (sleep < 0) {
	                sleep = 2;
	            }
	            
	            try {
	                Thread.sleep(sleep);
	            } catch (InterruptedException e) {
	            	Client.debugLog(e.getMessage());
	                break;
	            }

	            beforeTime = System.currentTimeMillis();
			}
		}
	}	
	
	public void sendKeyStatus() throws IOException {
		
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		Client.msgToServer("KeyData");
		
		stream.writeBoolean(upKey);
		stream.writeBoolean(downKey);
		stream.writeBoolean(leftKey);
		stream.writeBoolean(rightKey);
		stream.writeBoolean(JKey);
		stream.writeBoolean(KKey);
		stream.writeBoolean(LKey);
		
		Client.sendToServer( packet );
		
	}
	//OUTPUT TO SERVER***************************************************************************************************************************
	
	public synchronized void readUpdateData() throws IOException {
		
		if( drawing ) {
    		try {
				wait();
    		} catch (InterruptedException e) {
			}
    	}
		
		drawing = false;
		
		readScore();
		
		readGates();
		
		readStats();
		
		readCD();
		
		readUnits();
		
		readProjectiles();
		
		notify();
		
	}
	
	private void readScore() throws IOException {
		
		short bs, rs;
		bs = (short) Client.inDataStream.read();
		rs = (short) Client.inDataStream.read();
		if( blueScore == 0 && bs == 1) {
			scoreMoment = System.currentTimeMillis();
			scorer = true;
		}
		if( redScore == 0 && rs == 1 ) {
			scoreMoment = System.currentTimeMillis();
			scorer = false;
		}
		blueScore = bs;
		redScore = rs;
	}
	
	private void readGates() throws IOException {
		for(int i=0; i<map.traps.length; i++)
			map.traps[i].setActive(Client.inDataStream.readBoolean());
	}
	
	private void readStats() throws IOException {
		
		armor = (short) Client.inDataStream.read();
		damage = (short) Client.inDataStream.read();
		attTimer = Client.inDataStream.readShort();
		lifeSteal = (short) Client.inDataStream.read();
		speed = Client.inDataStream.readShort();
	}

	private void readCD() throws IOException {
		
		passiveCD = Client.inDataStream.read();
		passiveColor = Client.inDataStream.readBoolean();
		spellCD = Client.inDataStream.read();
		spellColor = Client.inDataStream.readBoolean();
		ultiCD = Client.inDataStream.read();
		ultiColor = Client.inDataStream.readBoolean();
	}

	private void readUnits() throws IOException {
		
		String name;
		String team;
		short x;
		short y;
		short sprite;
		short frame;
		short upD;
		short downD;
		short HPMax;
		short HP;
		
	    boolean visible;
	    
	    receivedUnits = new Unit[Client.inDataStream.read()];
	    
		for(int i=0; i<receivedUnits.length; i++) {
			
			if( Client.inDataStream.readBoolean() ) {		//if this unit was just created
				
				name = Client.readString();
				team = ""+Client.inDataStream.readChar();
				x = Client.inDataStream.readShort();
				y = Client.inDataStream.readShort();
				sprite = (short) Client.inDataStream.read();
				frame = (short) Client.inDataStream.read();
				upD = (short) Client.inDataStream.read();
				downD =(short) Client.inDataStream.read();
				HPMax = Client.inDataStream.readShort();
				HP = Client.inDataStream.readShort();
				visible = Client.inDataStream.readBoolean();
				
				receivedUnits[i] = new Unit(name, team, x, y, sprite, frame, upD, downD, HPMax, HP, visible);
			
			} else {			// this unit is already known, so it just needs to be updated
				
				x = Client.inDataStream.readShort();
				y = Client.inDataStream.readShort();
				frame = (short) Client.inDataStream.read();
				upD = (short) Client.inDataStream.read();
				HPMax = Client.inDataStream.readShort();
				HP = Client.inDataStream.readShort();
				visible = Client.inDataStream.readBoolean();
				
				units[i].update(x, y, frame, upD, HPMax, HP, visible);
				receivedUnits[i] = units[i];
			}
		}
		
		units = receivedUnits;
	}
	
	private void readProjectiles() throws IOException {
		
		receivedProj = new Projectile[Client.inDataStream.read()];
		
		if(receivedProj.length != 0) {
			short x;
		    short y;
		    short sprite;
		    short frame;
		    short range;
		    char team;
		   
		    for(short i=0; i<receivedProj.length; i++) {
		    
			    if( Client.inDataStream.readBoolean() ) {	//If the proj was just created
			    	
			    	team = Client.inDataStream.readChar();
			    	
			    	x = Client.inDataStream.readShort();
					y = Client.inDataStream.readShort();
					
					sprite = (short) Client.inDataStream.read();
					frame = (short) Client.inDataStream.read();
					
					range = Client.inDataStream.readShort();
					
					receivedProj[i] = new Projectile(x,y,sprite,frame,range,team);
					
			    } else {
			    	
			    	x = Client.inDataStream.readShort();
					y = Client.inDataStream.readShort();
					
					frame = (short) Client.inDataStream.read();
					
					projs[i].update(x, y, frame);
					receivedProj[i] = projs[i];
			    }
			}
		    
		    projs = receivedProj;
		    
		}
		else receivedProj = null;
	}
	
	/*private void updatePing() throws IOException {
		ping = Client.inDataStream.readInt();
	}*/
	
	//INPUT FROM SERVER***************************************************************************************************************************
}