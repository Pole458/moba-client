package game;

import javax.swing.ImageIcon;

public class Projectile {
	
	private short x;
	private short y;
	private String sprite;
	private short frame;
	private short range;
	private char team;
	
	public Projectile(short x, short y, short spriteNum, short frameIndex, short range, char team) {
		
		this.x=x;
		this.y=y;
		this.frame=frameIndex;
		sprite="sprites/Projectiles/";
		this.range=0;
		this.team='N';
		switch(spriteNum) {
		case 0:
			this.range = range;
			this.team = team;
			break;
		case 1:
			sprite+="1 - Bomb/Bomb_";
			break;
		case 2:
			sprite+="2 - Arrow/Arrow_";
			break;
		case 3:
			sprite+="3 - Crystal/Crystal_";
		break;
		case 4:
			sprite+="4 - Shred/Shred_";
		break;
		case 5:
			sprite+="5 - Claw/Claw_";
		break;
		}
	}
	
	public Projectile(Projectile p) {
		this.x = p.x;
		this.y = p.y;
		this.sprite = p.sprite;
		this.frame = p.frame;
		this.range = p.range;
		this.team = p.team;
	}
	
	public void update(short x, short y, short frame) {
		this.x = x;
		this.y = y;
		this.frame = frame;
	}
	
	public int getX() {
    	return x;
    }
    public int getY() {
    	return y;
    }
    public String getSprite() {
    	return sprite+String.valueOf(frame)+".png";
    }
    public int getWidth() {
    	return new ImageIcon(getSprite()).getImage().getWidth(null);
    }
    public int getHeight() {
    	return new ImageIcon(getSprite()).getImage().getHeight(null);
    }
    public short getRange() {
    	return range;
    }
    public char getTeam() {
    	return team;
    }
}