package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;

import menu.ClientPage;
import client.Client;

public class Unit {
	
	private String name;
	private String team;
    private int x;
    private int y;
   
    private short HPMax;
	private short HP;
	
	
	//drawing
	private short sprite;
	private short frame;
	private short downDistance;
	private short upDistance;
	private boolean visible;
	private ImageIcon image;
	
    public Unit(String name, String team, short x, short y, short sprite, short frame, short upDistance, short downDistance, short HPMax, short HP, boolean visible) {
		
    	this.name = name;
		if(team.equals("B")) this.team = "Blue";
		else this.team="Red";
		this.x = x;
		this.y = y;
		this.sprite = sprite;
		this.frame = frame;
		this.upDistance = upDistance;
		this.downDistance = downDistance;
    	this.HPMax = HPMax;
    	this.visible = visible;
    	this.HP = HP;
    	image = new ImageIcon(getSprite());
  
	}
    
	public Unit(Unit u) {
    	
		this.name = u.name;
    	this.team = u.team;
    	this.x = u.x;
    	this.y = u.y;
    	this.sprite = u.sprite;
    	this.frame = u.frame;
    	this.upDistance = u.upDistance;
    	this.downDistance = u.downDistance;
    	this.HPMax = u.HPMax;
    	this.HP = u.HP;
    	this.visible = u.visible;
    	this.image = u.image;
    }
	
	public void update(short x, short y, short frame, short upDistance, short HPMax, short HP, boolean visible) {
		
		this.x = x;
		this.y = y;
		this.frame = frame;
		this.upDistance = upDistance;
		this.HPMax = HPMax;
		this.HP = HP;
		this.visible = visible;
		image = new ImageIcon(getSprite());
		
	}
    
	public void draw(Graphics2D g2d, int inGameX, int inGameY) {
		
		if( getFrame() != -1)
			g2d.drawImage( getImage(),
					getX() - getImageWidth()/2 - inGameX,
					getY() - getDownDistance() - inGameY,
					null);
	
	}
	
	public void drawHealthBar(Graphics2D g2d, int inGameX, int inGameY, ImageObserver obv) {
		
		if(getName().equals("Pet")) {
			
			if(getFrame()!=-1 && getHP()>0) {
				g2d.setColor(Color.BLACK);
				
				g2d.fillRect(getX() - 21 -inGameX, getY() - getDownDistance() + getUpDistance() - 9 - inGameY, 42, 5);
				
				switch(getTeam()) {
				case "Blue":
					g2d.setColor(new Color(0,0,220));
					break;
				case "Red":
					g2d.setColor(Color.RED);
					break;
				}
				
				g2d.fillRect(getX() - 20 - inGameX, getY() - getDownDistance() + getUpDistance() - inGameY -8, 40*getHP()/getHPMax(), 3);
				
				//Damage received
				/*if(u.getDamageReceived()>0) {
					g2d.setColor(new Color(220,0,0));
					g2d.fillRect(u.getX()-20-inGameX+50*u.getHP()/u.getHPMax(), u.getY()-u.getHeight()-inGameY-8, 40*u.getDamageReceived()/u.getHPMax(), 3);
				}*/
			}
		} else {
			if( getFrame()!=-1 && getHP()>0) {
				
				g2d.setColor(Color.BLACK);
				ClientPage.drawStringCenter(g2d, getName(), getX() - inGameX, getY() - getDownDistance() + getUpDistance() - inGameY - 15);
				
				g2d.fillRect( getX() - 26 - inGameX, getY() - getDownDistance() + getUpDistance() - 9 - inGameY, 52,  8);
				
				switch(getTeam()) {
				case "Blue":
					g2d.setColor(new Color(0,0,220));
					break;
				case "Red":
					g2d.setColor(Color.RED);
					break;
				}
				
				g2d.fillRect(getX() - 25 - inGameX, getY() - getDownDistance() +  getUpDistance() - inGameY - 8, 50*getHP()/getHPMax(), 6);
				
				g2d.setColor(Color.BLACK);
				for(int i=1;i*25<getHPMax();i++)
					g2d.drawLine(getX()-25-inGameX+i*50*25/getHPMax(), getY()-getDownDistance() + getUpDistance()-inGameY-8, getX()-25-inGameX+i*50*25/getHPMax(), getY()-getDownDistance() + getUpDistance()-inGameY-5);
				for(int i=1;i*100<getHPMax();i++)
					g2d.drawLine(getX()-26-inGameX+i*50*100/getHPMax(), getY()-getDownDistance() + getUpDistance()-inGameY-8, getX()-26-inGameX+i*50*100/getHPMax(), getY()-getDownDistance() + getUpDistance()-inGameY-5);
				
				//Damage received
				/*if(u.getDamageReceived()>0) {
					g2d.setColor(new Color(220,0,0));
					g2d.fillRect(u.getX()-25-inGameX+50*u.getHP()/u.getHPMax(), u.getY()-u.getHeight()-inGameY-8, 50*u.getDamageReceived()/u.getHPMax(), 6);
				}*/
			}
		}
	}
	
    public int getX() {
    	return x;
    }
    public int getY() {
    	return y;
    }
    public int getFrame() {
    	return frame;
    }
    
    public Image getImage() {
    	
    	return image.getImage();
    }
    
    public String getSprite() {
    	if(frame==-1) return null;
    	else return Client.getPlayerSprite(sprite, frame);
    }
    
    public String getName() {
    	return name;
    }
    
    public String getTeam() {
    	return team;
    }
    
    public boolean getVisible() {
    	return visible;
    }
    
    public int getImageHeight() {
    	return getImage().getHeight(null);
	}
	
    public int getImageWidth() {
    	
		return getImage().getWidth(null);
	}
	
    public short getDownDistance() {
    	return downDistance;
	}
    
    public short getUpDistance() {
    	return upDistance;
    }
    
    public int getHPMax() {
		return HPMax;
	}
    
	public int getHP() {
		return HP;
	}
	
}