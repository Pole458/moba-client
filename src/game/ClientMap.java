package game;

import java.awt.Image;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.ImageIcon;

public class ClientMap {
	public short cells[][][];
	public static final int cellSize=24;
	public static int tileX;
	public static int tileY;
	public ClientTrap[] traps;
	
	public ClientMap() {
		
		try {
			ObjectInputStream stream = new ObjectInputStream(new FileInputStream("map.bin"));
			cells = (short[][][])stream.readObject();
			stream.close();
			tileX=0;
			while(true)
				try {
					if(cells[0][tileX][0]<1000)	tileX++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
			tileY=0;
			while(true)
				try {
					if(cells[0][0][tileY]<1000) tileY++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
						
		} catch(ClassNotFoundException exception) {	
		} catch (IOException e) {
		}
		
	}
	
	public Boolean getCell(int x, int y) {
		if(x>=0 && x<tileX && y>=0 && y<tileY) {
			if(cells[2][x][y]!=100) return false;
			return true;
		}
		return false;
	}
	
	public Image drawCell(int l, int x, int y) {
		if(l>=0 && l<4 && x>=0 && x<tileX && y>=0 && y<tileY && cells[l][x][y]!=100)
			return new ImageIcon(("TileSet/tile_"+cells[l][x][y]+".png")).getImage();
		else
			return null;
	}
	
}