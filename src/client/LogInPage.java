package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import menu.ClientPage;
import menu.MyButton;

@SuppressWarnings("serial")
public class LogInPage extends ClientPage {
	
	private Client client;
	private JTextField machineField;
	private JTextField portField;
	private JTextField nameField;
	private MyButton LogInButton;
	private JTextField errorArea;
	
	public LogInPage(Client c) {
		
		super();
		
		client = c;
		machineField = new JTextField("0.0.0.0");
		portField = new JTextField("1900");
		nameField = new JTextField("Name");
		errorArea = new JTextField("! - Name Already Used - !");
		errorArea.setEditable(false);
		errorArea.setVisible(false);
		
		nameField.setBounds(190,25,70,25);
		machineField.setBounds(140,75,120,25);
		portField.setBounds(210,125,50,25);
		errorArea.setBounds(50,175,200,25);
		
		add(machineField);
		add(portField);
		add(nameField);
		add(errorArea);
		
		machineField.setHorizontalAlignment(SwingConstants.RIGHT);
		portField.setHorizontalAlignment(SwingConstants.CENTER);
		nameField.setHorizontalAlignment(SwingConstants.CENTER);
		errorArea.setHorizontalAlignment(SwingConstants.CENTER);
		
		machineField.setBackground(Color.DARK_GRAY);
		portField.setBackground(Color.DARK_GRAY);
		nameField.setBackground(Color.DARK_GRAY);
		errorArea.setBackground(Color.DARK_GRAY);
		
		machineField.setForeground(Color.BLACK);
		portField.setForeground(Color.BLACK);
		nameField.setForeground(Color.BLACK);
		errorArea.setForeground(new Color(0,154,215));
		
		Border border = BorderFactory.createLineBorder(new Color(0,154,215));
		
		machineField.setBorder(border);
		portField.setBorder(border);
		nameField.setBorder(border);
		
		border = BorderFactory.createLineBorder(Color.DARK_GRAY);
		errorArea.setBorder(border);
		
		LogInButton = new MyButton("Log In",new Color(0,154,215),new Rectangle(100,205,100,25));
	}
	
	public void paint(Graphics g) {
		
		g.setFont(new Font("Look",Font.PLAIN,16));
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		g.setColor(Color.BLACK);
		g.drawString("Name:", 50, 46);
		g.drawString("Server:", 50, 96);
		g.drawString("Port:", 50, 146);
		
		g.setColor(new Color(0,154,215));
		g.drawLine(45,55,265,55);
		g.drawLine(45,105,265,105);
		g.drawLine(45,155,265,155);
		
		if(LogInButton!=null) LogInButton.draw(g);
		
		paintComponents(g);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(LogInButton.getSize().contains(e.getX(),e.getY()) ) {
			if(nameField.getText().length() <= 10)
				client.connectToServer(getMachine(), getPort(), getName());		//Calls the method to connect with the server
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(LogInButton.getSize().contains(e.getX(),e.getY()) ) {
			if(!LogInButton.isSelected()) LogInButton.setSelected(true);
		} else {
			if(LogInButton.isSelected())	LogInButton.setSelected(false);
		}
	}

	public String getMachine() {
		return machineField.getText();
	}
	public int getPort() {
		return Integer.valueOf(portField.getText());
	}
	public String getName() {
		return nameField.getText();
	}
	
	public void setErrorMessage(String s) {
		if(s == "") errorArea.setVisible(false);
		else {
			errorArea.setVisible(true);
			errorArea.setText("! - " + s + " - !");
		}
	}
	
}