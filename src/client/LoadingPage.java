package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import menu.ClientPanel;

@SuppressWarnings("serial")
public class LoadingPage extends ClientPanel {
	private String text;
	private short frame;
	
	public LoadingPage(String s) {
		
		super();
		
		text=s;
		frame=0;
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		g.setFont(new Font("Look",Font.PLAIN,18));
		
		frame++;
		if(frame>60) frame=0;
		
		g.setColor(new Color(0,154,215));
		g.fillOval(getWidth()/2-100,getHeight()/2-100,200,200);
		
		g.setColor(Color.DARK_GRAY);
		g.fillOval(getWidth()/2-95,getHeight()/2-95,190,190);
		g.fillArc(getWidth()/2-100,getHeight()/2-100,200,200,(-frame*6)%360,20);
		
		g.setColor(new Color(0,154,215));
		g.drawString(text,getWidth()/2-text.length()*4,getHeight()/2+6);
	}
	
	public void setText(String s) {
		text=s;
	}
}