package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import menu.ClientPage;
import menu.ImageButton;
import menu.MyButton;

@SuppressWarnings("serial")
public class ChampionSelectPage extends ClientPage {

	private MyButton readyButton;
	private ImageButton[] playersButtons;
	private ImageButton[] champsButtons;
	private short playerSelecting;
	private short champSelected;
	private String[] infos;
	
	private String[] playersNames;
	private short[] champions;
	
	private long startTime;
	
	public ChampionSelectPage(String[] names, boolean[] teams) {
		
		super();
		
		startTime = System.currentTimeMillis();
		
		playerSelecting = 0;
		
		champSelected = -1;
		
		playersNames = names;
		
		champions = new short[playersNames.length];
		
		playersButtons = new ImageButton[playersNames.length];
		
		for(short b=0, r=0; b+r<champions.length;) {
			champions[b+r] = -1;
			if(teams[b+r]) {
				playersButtons[b+r] = new ImageButton(playersNames[b+r], "", Color.BLUE,new Rectangle(25 + 75 * b, 0, 75, 75));
				b++;
			}
			else {
				playersButtons[b+r] = new ImageButton(playersNames[b+r], "", Color.RED,new Rectangle(500 - 75 * r, 0, 75, 75));
				r++;
			}
		}
		
		if(playersNames[playerSelecting].equals(Client.UserName))
			createReadyButton();
		
		champsButtons = new ImageButton[12];
		champsButtons[0] = new ImageButton("","sprites/Champions/Player1/Player1_0.png",new Color(0,154,215),new Rectangle(0,100,75,100));
		champsButtons[1] = new ImageButton("","sprites/Champions/Player2/Player2_0.png",new Color(0,154,215),new Rectangle(75,100,75,100));
		champsButtons[2] = new ImageButton("","sprites/Champions/Player3/Player3_0.png",new Color(0,154,215),new Rectangle(150,100,75,100));
		champsButtons[3] = new ImageButton("","sprites/Champions/Player4/Player4_0.png",new Color(0,154,215),new Rectangle(225,100,75,100));
		champsButtons[4] = new ImageButton("","sprites/Champions/Player5/Player5_0.png",new Color(0,154,215),new Rectangle(300,100,75,100));
		champsButtons[5] = new ImageButton("","sprites/Champions/Player6/Player6_0.png",new Color(0,154,215),new Rectangle(375,100,75,100));
		champsButtons[6] = new ImageButton("","sprites/Champions/Player7/Player7_0.png",new Color(0,154,215),new Rectangle(0,200,75,100));
		champsButtons[7] = new ImageButton("","sprites/Champions/Player8/Player8_0.png",new Color(0,154,215),new Rectangle(75,200,75,100));
		champsButtons[8] = new ImageButton("","sprites/Champions/Player9/Player9_0.png",new Color(0,154,215),new Rectangle(150,200,75,100));
		champsButtons[9] = new ImageButton("","sprites/Champions/Player10/Player10_0.png",new Color(0,154,215),new Rectangle(225,200,75,100));
		champsButtons[10] = new ImageButton("","sprites/Champions/Player11/Player11_0.png",new Color(0,154,215),new Rectangle(300,200,75,100));
		champsButtons[11] = new ImageButton("","sprites/Champions/Player12/Player12_0.png",new Color(0,154,215),new Rectangle(375,200,75,100));
		
		infos = new String[3];
		infos[0] = new String("Passive");
		infos[1] = new String("Spell");
		infos[2] = new String("Ulti");
	}

	public void paint(Graphics g) {
		
		g.setFont(new Font("Look",Font.PLAIN,14));
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		if(champsButtons!=null)
			for(short i=0; i<champsButtons.length;i++) 
				champsButtons[i].draw(g);
		
		if(playersButtons!=null)
			for(short i=0; i<playersButtons.length; i++)
				playersButtons[i].draw(g);
		
		g.setColor(new Color(0,154,215));
		
		g.setColor(Color.BLUE);
		g.drawLine(0,80,275,80);
		g.setColor(Color.RED);
		g.drawLine(325,80,600,80);
		
		g.setColor(new Color(0,154,215));
		
		g.drawRect(0,310,200,289);
		g.drawRect(200,310,200,289);
		g.drawRect(400,310,200,289);
		
		drawInfo(g);
		
		if(playerSelecting == playersNames.length) {	//if all the champions have been selected
			if(System.currentTimeMillis() < startTime) {
				g.setFont(new Font("Look",Font.PLAIN,25));
				ClientPage.drawStringCenter(g, ""+(int)((startTime - System.currentTimeMillis())/1000), 300, 50);
			}
		}
	
		if(readyButton!=null) readyButton.draw(g);
		
	}
	
	public void drawInfo(Graphics g) {
		
		Polygon p = new Polygon();
	    for (int i = 0; i < 5; i++)
	    	p.addPoint((int)(525+50*Math.cos(i*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*Math.sin(i*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
		g.drawPolygon(p);
		
		g.setFont(new Font("Look",Font.PLAIN,15));
		
		switch(champSelected) {
		case -1:
			break;
		case 0:
			ClientPage.drawStringCenter(g, "Nightdart", 525, 95);
			drawPentagon(g,p,(short)20,(short)60,(short)60,(short)12,(short)18);
			break;
		case 1:
			ClientPage.drawStringCenter(g, "Purprowl", 525, 95);
			drawPentagon(g,p,(short)100,(short)20,(short)60,(short)24,(short)27);
			break;
		case 2:
			ClientPage.drawStringCenter(g, "Bullbot", 525, 95);
			drawPentagon(g,p,(short)20,(short)20,(short)10,(short)94,(short)72);
			break;
		case 3:
			ClientPage.drawStringCenter(g, "Hatwitch", 525, 95);
			drawPentagon(g,p,(short)60,(short)20,(short)20,(short)53,(short)5);
			break;
		case 4:
			ClientPage.drawStringCenter(g, "Lord Agony", 525, 95);
			drawPentagon(g,p,(short)4,(short)50,(short)100,(short)24,(short)64);
			break;
		case 5:
			ClientPage.drawStringCenter(g, "Blurred", 525, 95);
			drawPentagon(g,p,(short)20,(short)20,(short)4,(short)76,(short)63);
			break;
		case 6:
			ClientPage.drawStringCenter(g, "Steamstoove", 525, 95);
			drawPentagon(g,p,(short)34,(short)20,(short)10,(short)35,(short)55);
			break;
		case 7:
			ClientPage.drawStringCenter(g, "Swordensheeld", 525, 95);
			drawPentagon(g,p,(short)100,(short)20,(short)10,(short)41,(short)64);
			break;
		case 8:
			ClientPage.drawStringCenter(g, "Onslaught", 525, 95);
			drawPentagon(g,p,(short)40,(short)20,(short)60,(short)88,(short)27);
			break;
		case 9:
			ClientPage.drawStringCenter(g, "Spike", 525, 95);
			drawPentagon(g,p,(short)20,(short)40,(short)10,(short)71,(short)27);
			break;
		case 10:
			ClientPage.drawStringCenter(g, "Mastermind", 525, 95);
			drawPentagon(g,p,(short)20,(short)60,(short)4,(short)82,(short)13);
			
			break;
		case 11:
			ClientPage.drawStringCenter(g, "Bullseye", 525, 95);
			drawPentagon(g,p,(short)60,(short)30,(short)80,(short)18,(short)9);
			break;
		}
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("Look",Font.PLAIN,10));
		g.drawString("Damage",(int)(525+50*Math.cos(0*2*Math.PI/5+(Math.PI/2-Math.PI/5))-12),(int)(250+50*Math.sin(0*2*Math.PI/5+(Math.PI/2-Math.PI/5)))+8);
		g.drawString("Att.Spd",(int)(525+50*Math.cos(1*2*Math.PI/5+(Math.PI/2-Math.PI/5))-14),(int)(250+50*Math.sin(1*2*Math.PI/5+(Math.PI/2-Math.PI/5)))+8);
		g.drawString("LifeSteal",(int)(525+50*Math.cos(2*2*Math.PI/5+(Math.PI/2-Math.PI/5))-12),(int)(250+50*Math.sin(2*2*Math.PI/5+(Math.PI/2-Math.PI/5)))-4);
		g.drawString("HP",(int)(525+50*Math.cos(3*2*Math.PI/5+(Math.PI/2-Math.PI/5)))-4,(int)(250+50*Math.sin(3*2*Math.PI/5+(Math.PI/2-Math.PI/5)))-4);
		g.drawString("Armor",(int)(525+50*Math.cos(4*2*Math.PI/5+(Math.PI/2-Math.PI/5))-12),(int)(250+50*Math.sin(4*2*Math.PI/5+(Math.PI/2-Math.PI/5)))-4);
		
		g.setFont(new Font("Look",Font.PLAIN,10));
		for(short i=0; i<infos.length; i++) {
			drawStringMultipleLines(g, infos[i], 2 + 200 * i, 320, 190);
		}
		
		paintComponents(g);
		
	}
	
	public void drawPentagon(Graphics g,Polygon p, short p1, short p2, short p3, short p4, short p5) {
		p = new Polygon();
	    p.addPoint((int)(525+50*p1/100*Math.cos(0*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*p1/100*Math.sin(0*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
	    p.addPoint((int)(525+50*p2/100*Math.cos(1*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*p2/100*Math.sin(1*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
	    p.addPoint((int)(525+50*p3/100*Math.cos(2*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*p3/100*Math.sin(2*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
	    p.addPoint((int)(525+50*p4/100*Math.cos(3*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*p4/100*Math.sin(3*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
	    p.addPoint((int)(525+50*p5/100*Math.cos(4*2*Math.PI/5+(Math.PI/2-Math.PI/5))),(int)(250+50*p5/100*Math.sin(4*2*Math.PI/5+(Math.PI/2-Math.PI/5))));
	    g.fillPolygon(p);
	}
	
	public void changeSpellAreas() {
		switch(champSelected) {
		case 0:
			infos[0] = new String("Poison Darts\nEvery autoattack on the same target deals damage based on target maximum health, increasing up to 5 times.");
			infos[1] = new String("Adjustable Crossbow\nNightdart can adjust his crossbow, swithcing between more Att.Spd or more Lifesteal.");
			infos[2] = new String("Power Recoil\nEvery 3 autoattacks he charges a stack of 'Heavy Dart': he can spend this charge causing him to be bounced back by the recoil.");
			break;
		case 1:
			infos[0] = new String("Prey the Weak\nPurprowl detects the weakest enemy near him. His autoattacks against him will deal more damage.");
			infos[1] = new String("Ground Fist\nPurprowl fists the ground, dealing a big amount of damage that is shared between the enemies hit.");
			infos[2] = new String("Dig Assault\nPurprowl dives the terrain, unborrowing at the weakest enemy position and dealing consistent damage.");
			break;
		case 2:
			infos[0] = new String("Hammer Blow\nEvery 10 seconds, Bullbot next autoattack will deal damage based on Bullbot's maximin Health, stunning the target ");
			infos[1] = new String("Hammer Dance\nBullbot rotates his hammer, knocking back all the enemies and damaging them scaling on Bullbot's Armor.");
			infos[2] = new String("Titan\nBullbot gains bonus maximum Health, Armor and Damage for 10 seconds.");
			break;
		case 3:
			infos[0] = new String("Crystal Power\nEvery time he hits a enemy with a spell, he gains a charge of 'Crystal'. His next AA will deal damage based on the stack in an area around the target.");
			infos[1] = new String("Bouncing Crystal\nHe throws a Crystal that bounce between the enemies for a maximum of 4 bounces.");
			infos[2] = new String("Dangerous Crystal\nHe puts a Crystal on the neareast between allies and enemies. The Crystal deals a big amount of damage in an area along 5 seconds.");
			break;
		case 4:
			infos[0] = new String("Pact with the Death\nWhen he dies, he goes in stasis for 2 seconds, damaging and lifestealing all around himself.");
			infos[1] = new String("Masochism\nWhen active, he constantly loses health, but he gains Att.Spd based on how much health is missing.");
			infos[2] = new String("Rage of the Dead\nFor 8 seconds, every autoattacks will deal damage in an area around him.");
			break;
		case 5:
			infos[0] = new String("Plasma\nEvery time Blurred swithces form, his next autoattack will have a special effect.");
			infos[1] = new String("Rheology\nSolid form: he gains bonus Armor, and his next autoattack will stun the target.\nLiquid form: he gains bonus Damage, and his next autoattack will deal more damage.");
			infos[2] = new String("Unstable Plasm\nHe becames invisible until 8 seconds pass or when riactivated. When returning visible, he damages enemies in an area around him, knocking them back.");
			break;
		case 6:
			infos[0] = new String("Choo Choo!\nWhile moving he charges Vapor, which increases his Armor and Mvm.Spd. When standing still, he looses his Vapor. Also his autoattack damages in an area.");
			infos[1] = new String("Vapor Vent\nHe consumes all his Vapor in order to speed every allies near him and slow every enemies in range.");
			infos[2] = new String("VaporCleaner\nCan be activeted only at 100 Vapor: he attracts every enemy hit near him, consuming all his Vapor.");
			break;
		case 7:
			infos[0] = new String("Stamina\nStamina permits him to use his spells. He regenerates Stamina while not using spell.");
			infos[1] = new String("Shield to Block\nHe holds up his shield, increasing his Armor by an amount based on his Stamina. He can't attack while holding up the shield, and receiving damage reduces his Stamina.");
			infos[2] = new String("Sword to Blow\nHe raises his sword to strike a powerful blow around him. While charging he consumes Stamina, but he increases the damage.");
			break;
		case 8:
			infos[0] = new String("Bloody Vapor\nEvery autoattack realeses 10% Vapor. At 75% Vapor, his next autoattack will realese 50% Vapor, knocking back the target.");
			infos[1] = new String("Vapor Cannons\nWhen active, he damages enemies around him, generating 10% Vapor per second. He heals himself if he damages an enemy.If Vapor reaches 100%, he's stunned for 4 seconds.");
			infos[2] = new String("Onslaught\nOnslaught deserves his names: he gains 100% bonus Lifesteal, MvmSpd based on his Vapor percentage and he's immune to crowd controls for 8 seconds.");
			break;
		case 9:
			infos[0] = new String("Spear up\nEach autoattack increases his Att.Spd, for a maximum of 5 times.");
			infos[1] = new String("Transfix\nSpike dashes in the direction of the nearest enemy. He can dash again in the next two seconds, but he cant dash on the same target twice. It also apply on-hit effects");
			infos[2] = new String("Pincushion\nSpike assaults the nearest enemies, ignoring the ones hit by his spell. The target his stunned for 2 seconds and Spike autoattacks him 4 times.");
			break;
		case 10:
			infos[0] = new String("Captain\nMastermind gains Att.Spd for every ToySoldier alive. Each autoattack reduces his 'Puppeter' cooldown by 10%");
			infos[1] = new String("Puppeter\nHe summons a ToySoldier. He can have a maximum of 3 ToySoldiers. ToySoldiers dies when Mastermind goes to far.");
			infos[2] = new String("Commander\nHolding this key permits him to give order to his ToySoldiers. With K+L he orders them to autoattack. With J+L he orders them to dash in the direction he's facing.");
			break;
		case 11:
			infos[0] = new String("Long Bow\nBullseye is a proud archer: infact he deals more damage when the target is far from him, and deal less damage when the target is too near.");		
			infos[1] = new String("Hawk Eye\nFor 6 seconds, all enemies near Bullseye's maximum range have their Armor reduced.");
			infos[2] = new String("Virtuosity Archey\nFor 8 seconds, he will fire 2 arrows for every autoattacks.");
			break;
		}
		
	}
	
	public void drawStringMultipleLines(Graphics g, String s, int x, int y, int w) {
		String ts="";
		short l=0;
		for(short i=0; i<s.length() ; i++) {
			if(g.getFontMetrics().stringWidth(ts)<w && s.charAt(i)!='\n') {
				ts+=s.charAt(i);
			} else {
				g.drawString(ts,x,y+l*g.getFontMetrics().getHeight());
				ts=""+s.charAt(i);
				l++;
			}
		}
		if(!ts.equals(""))g.drawString(ts,x,y+l*g.getFontMetrics().getHeight());
	}
	
	private void createReadyButton() {
		readyButton = new MyButton("READY", new Color(0,154,215), new Rectangle(260, 25, 80, 30) );
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(readyButton!=null)
			if(readyButton.getSize().contains(e.getX(),e.getY()) ) {
				if(champSelected!=-1) {
					try {
						
						Client.msgToServer("ChampionSelected");
						
						ByteArrayOutputStream packet = new ByteArrayOutputStream();
						DataOutputStream stream = new DataOutputStream(packet);
						
						stream.writeShort(champSelected);
						Client.sendToServer( packet );
						
						readyButton = null;
					
					} catch (IOException e1) {
						Client.debugLog(e1.getMessage());
					}
				}
			}
		
		if(champsButtons!=null)
			for(short i=0; i<champsButtons.length;i++)
				if(champsButtons[i].getSize().contains(e.getX(),e.getY()) ) {
					champsButtons[i].setSelected(true);
					for(short j=0; j<champsButtons.length;j++)
						if(j!=i) champsButtons[j].setSelected(false);
					champSelected = i;
					changeSpellAreas();
					break;
				}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		
		if(readyButton!=null)
			if(readyButton.getSize().contains(e.getX(),e.getY()) ) {
				if(!readyButton.isSelected()) readyButton.setSelected(true);
			} else {
				if(readyButton.isSelected()) readyButton.setSelected(false);
			}
		
	}
	
	public int getPlayers() {
		return playersNames.length;
	}
	
	public void setChampion(short c) {
		
		champions[playerSelecting] = c;
	
		playersButtons[playerSelecting].setImage("sprites/Champions/Player"+(c+1)+"/Player"+(c+1)+"_0.png");
		
		playerSelecting++;
		
		if(playerSelecting<playersNames.length) {
			if(playersNames[playerSelecting].equals(Client.UserName))	//if it's my turn to pick
				createReadyButton();
		} else {	//If all the champions have already been picked
			startTime = System.currentTimeMillis() + 5000;
		}

		
	}
}