package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import menu.ClientPage;
import menu.ClientPanel;
import menu.MyButton;

@SuppressWarnings("serial")
public class InvitePanel extends ClientPanel {

	private MyButton acceptButton;
	private MyButton declineButton;
	
	private String inviter;
	
	public InvitePanel(String i) {
		
		super();
		
		setBounds(400, 300, 200, 100);
		
		inviter = i;
		
		acceptButton = new MyButton("Accept",new Color(0,154,215),new Rectangle(10,60,80,25));
		declineButton = new MyButton("Decline",new Color(0,154,215),new Rectangle(110,60,80,25));
	
	}
	
	public void paint(Graphics g) {
		
		g.setFont(new Font("Look",Font.PLAIN,12));
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setColor(new Color(0,154,215));
		g.drawRect(0, 0, getWidth()-1, getHeight()-1);
		g.drawRect(1, 1, getWidth()-3, getHeight()-3);

		ClientPage.drawStringCenter(g, inviter, getWidth()/2, 20);
		ClientPage.drawStringCenter(g, "invited you to a game", getWidth()/2, 40);
	
		acceptButton.draw(g);
		declineButton.draw(g);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(acceptButton.getSize().contains(e.getX(),e.getY()) ) {
			Client.msgToServer("InviteAccepted");			//Tells the server to accept the invite
			Client.writeString(inviter); 					//Tells the server which user invited him
			Client.invitePanel = null;						//Removes the InvitePanel
			((ClientPage)getParent()).invitePanel = null;	
			getParent().remove(this);
		}
		
		if(declineButton.getSize().contains(e.getX(),e.getY()) ) {
			Client.invitePanel = null;						//Removes the InvitePanel
			((ClientPage)getParent()).invitePanel = null;	
			getParent().remove(this);
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(acceptButton!=null)
			if(acceptButton.getSize().contains(e.getX(),e.getY())) acceptButton.setSelected(true);
			else acceptButton.setSelected(false);
		
		if(declineButton!=null)
			if(declineButton.getSize().contains(e.getX(),e.getY())) declineButton.setSelected(true);
			else declineButton.setSelected(false);
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
		acceptButton.setSelected(false);
		declineButton.setSelected(false);
	}
	
}
