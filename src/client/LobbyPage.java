package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import menu.ClientPage;
import menu.MyButton;

@SuppressWarnings("serial")
public class LobbyPage extends ClientPage {
	
	private MyButton startGameButton;
	private MyButton exitLobbyButton;
	private MyButton inviteButton;
	private JTextField inviteTextField;
	private String inviteError;
	
	private MyButton changeTeamButton;
	
	private String[] userNames;
	private boolean[] teams;
	
	public LobbyPage (String[] n, boolean t[]) {
		
		super();
		
		updateLobby(n, t);
		
		exitLobbyButton = new MyButton("X", new Color(0,154,215), new Rectangle(575,25,20,20));
		
		inviteError = "";
		
	}
	
	public void updateLobby(String[] n, boolean[] t) {
		
		userNames = n;
		teams = t;
		
		
		//understand the team of the user
		boolean team = true;
		for(short i=0; i<userNames.length; i++ )
			if( userNames[i].equals(Client.UserName) ) {
				team = teams[i];
				break;
		}
		
		//understand if the user can switch team
		short c = 0;
		for(short i=0; i<userNames.length; i++ )
			if( team != teams[i] )
				c++;
		
		//if there is enough room in the other team, create the button with the right direction
		if(c<3) 
			if(team)
				changeTeamButton = new MyButton(">", new Color(0,154,215), new Rectangle(285,85,30,30));
			else
				changeTeamButton = new MyButton("<", new Color(0,154,215), new Rectangle(285,85,30,30));
		
		if(userNames[0].equals(Client.UserName) && startGameButton == null) {	//If the user is the owner of the lobby
			
			startGameButton = new MyButton("START", new Color(0,154,215), new Rectangle(250,25,100,25));
			inviteButton = new MyButton("Invite", new Color(0,154,215), new Rectangle(450,330,100,25));
			
			inviteTextField = new JTextField("");
			inviteTextField.setBounds(425,360,150,25);
			add(inviteTextField);
			inviteTextField.setHorizontalAlignment(SwingConstants.RIGHT);		
			inviteTextField.setBackground(Color.DARK_GRAY);
			inviteTextField.setForeground(Color.BLACK);
			inviteTextField.setBorder( BorderFactory.createLineBorder(new Color(0,154,215)));
			
		} else {
			startGameButton = null;
			inviteTextField = null;
		}
	
	}
	
	public void paint(Graphics g) {
		
		g.setFont(new Font("Look",Font.PLAIN,13));
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		g.setColor(new Color(0,154,215));
		
		//Invite error string
		ClientPage.drawStringCenter(g, inviteError, 500, 320);
		
		//vertical center line
		g.drawLine(300, 120, 300, 300);
		
		//horizontal name lines
		g.drawLine(50, 100, 275, 100);
		g.drawLine(325, 100, 550, 100);
		
		ClientPage.drawStringCenter(g, "Blue Team", 150, 90);
		ClientPage.drawStringCenter(g, "Red Team", 450, 90);
		
		for(short blue = 0, red = 0; blue+red<userNames.length; ) {
			
			if(teams[blue+red]) {	//If blue team
				ClientPage.drawStringCenter(g, userNames[blue+red], 150, 130 + blue * 45 );
				g.drawLine(100, 140 + blue * 45, 200, 140 + blue * 45 );
				blue++;
			} else { 			//if red team
				ClientPage.drawStringCenter(g, userNames[blue+red], 450, 130 + red * 45);
				g.drawLine(400, 140 + red * 45, 500, 140 + red * 45);
				red++;
			}
		}
		
		if(startGameButton != null) startGameButton.draw(g);
		if(exitLobbyButton != null) exitLobbyButton.draw(g);
		if(inviteButton != null) inviteButton.draw(g);
		if(changeTeamButton != null) changeTeamButton.draw(g);
		
		paintComponents(g);
	}
	
	public void setInviteError(String s) {
		inviteError = s;
	}
	
	public boolean inLobby(String s) {
		for(short i=0; i<userNames.length; i++)
			if(userNames[i].equals(s)) return true;
		
		return false;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(exitLobbyButton.getSize().contains(e.getX(),e.getY()) ) {
			Client.msgToServer("ExitLobby");
		}
		
		if(startGameButton != null) {
			if(startGameButton.getSize().contains(e.getX(),e.getY()) ) {
				Client.msgToServer("ChampionSelect");
			}
		}
		
		if(inviteButton != null)
			if(inviteButton.getSize().contains(e.getX(),e.getY()) ) {
				if(!inLobby(inviteTextField.getText()) && !inviteTextField.getText().equals("") ) {					//This prevent the user from inviting someone already in this lobby
					Client.msgToServer("InviteToLobby");					//Tell the server to send an invite
					Client.writeString(inviteTextField.getText());			//Tell the server the name of the invited user							
				}
				inviteTextField.setText("");		//Cleans the inviteTextField
				setInviteError("");
			}
		
		if(changeTeamButton != null) {
			if(changeTeamButton.getSize().contains(e.getX(),e.getY()) ) {
				Client.msgToServer("SwitchTeam");
				changeTeamButton = null;
			}
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		
		if(exitLobbyButton.getSize().contains(e.getX(),e.getY()) ) {
			if(!exitLobbyButton.isSelected()) exitLobbyButton.setSelected(true);
		} else {
			if(exitLobbyButton.isSelected()) exitLobbyButton.setSelected(false);
		}
		
		if(startGameButton != null)
			if(startGameButton.getSize().contains(e.getX(),e.getY()) ) {
				if(!startGameButton.isSelected()) startGameButton.setSelected(true);
			} else {
				if(startGameButton.isSelected()) startGameButton.setSelected(false);
			}
		
		if(inviteButton != null)
			if(inviteButton.getSize().contains(e.getX(),e.getY()) ) {
				if(!inviteButton.isSelected()) inviteButton.setSelected(true);
			} else {
				if(inviteButton.isSelected()) inviteButton.setSelected(false);
			}
		
		if(changeTeamButton != null)
			if(changeTeamButton.getSize().contains(e.getX(),e.getY()) ) {
				if(!changeTeamButton.isSelected()) changeTeamButton.setSelected(true);
			} else {
				if(changeTeamButton.isSelected()) changeTeamButton.setSelected(false);
			}
	}

}
