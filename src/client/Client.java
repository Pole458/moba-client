package client;

import game.ClientGame;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import javax.swing.JFrame;
import menu.ClientPage;
import menu.ClientPanel;

@SuppressWarnings("serial")
public class Client extends JFrame implements Runnable, WindowListener {
	
	//DEBUG
	public static boolean debug = false;
	
	private final int DRAWDELAY = 1000 / 30;	//30 fps
	
	public static String Status;
	
	public InputStream inStream;
	public static DataInputStream inDataStream;
	
	public OutputStream outStream;
	private static DataOutputStream outDataStream;
	
	public static boolean sending;
	
	private Socket connection;
	
	private static String ServerIp;
	private boolean connected;
	private int port;
	
	public static String UserName;
	
	private ClientPage currentPage;
	
	private ClosePanel closePanel;
	
	public static ClientPanel invitePanel;
	
	private Thread paintThread;
	
	private Thread inputThread;
	
	public Client() {
		
		Status = "LogInPage";
		
		connected = false;
		
		sending = false;
		
		setSize(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("MOBA - Client");
        setResizable(false);
        setLocationRelativeTo(null);
        setUndecorated(true);
        setVisible(true);
        
        addKeyListener(new TAdapter());
        
    	closePanel = new ClosePanel(this);
		add(closePanel);
        
        paintThread = new Thread(this, "Client Paint Thread");
		paintThread.start();
		
		createLogInPage();
	}
	
	//CLIENT MAIN THREAD**********************************************************************************************		
	@Override
	public void run() {
		long beforeTime, timeDiff, sleep;
		
		beforeTime = System.currentTimeMillis();
		
		while(true) {
			
			repaint();
			
			timeDiff = System.currentTimeMillis() - beforeTime;
	        sleep = DRAWDELAY - timeDiff;
		
	        if (sleep < 0) {
                sleep = 2;
            }
	        
	        
	        try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
            	Client.debugLog(e.getMessage());
                break;
            }

            beforeTime = System.currentTimeMillis();
				
		}
	}
	//CLIENT MAIN THREAD**********************************************************************************************				
	//CLIENT PAINT**********************************************************************************************
	public void paint(Graphics g) {
		
		if(currentPage != null) {
			currentPage.setBounds(0,25,getWidth(),getHeight()-25);
			currentPage.repaint();
		}
	
		if(closePanel!=null) {
			closePanel.setBounds(0,0,getWidth(),25);
			closePanel.repaint();
		}
	}

	public void createLogInPage() {
		
		Client.debugLog("LogInPage");
		
		if( currentPage != null ) remove(currentPage);
		currentPage = new LogInPage(this);
		add(currentPage);
	}
	
	public void createMainPage() {
		
		remove(currentPage);
		currentPage = new MainPage(currentPage);
		add(currentPage);
		
		Client.debugLog("MainPage");
	}
	
	public void createLobbyPage() throws IOException {
		
		Client.debugLog("Receiving Lobby data...");
		
		short currentPlayers = inDataStream.readShort();
		
		String[] names = new String[currentPlayers];
		boolean[] team = new boolean[currentPlayers];
		
		for(short i=0; i<currentPlayers; i++) {
			
			names[i] = Client.readString();
			team[i] = inDataStream.readBoolean();
			
			Client.debugLog("User: " + names[i] + ", team " + team[i]);
		}
		
		remove(currentPage);
		currentPage = new LobbyPage(names, team);
		add(currentPage);
		
		Client.debugLog("LobbyPage");
	}
	
	public void createChampionSelectPage() throws IOException {
		
		Client.debugLog("Receiving CreatedChampionSelect data...");
		
		short players = inDataStream.readShort();
		
		String[] names = new String[players];
		boolean[] team = new boolean[players];
		
		for(short i=0; i<players; i++) {
			
			names[i] = Client.readString();
			team[i] = inDataStream.readBoolean();
			
			Client.debugLog("User: " + names[i] + ", team: " + team[i]);
		}
		
		remove(currentPage);
		currentPage = new ChampionSelectPage(names, team);
		add(currentPage);
		
		Client.debugLog("ChampionSelectPage");
	}
	
	public void updateChampionSelect() throws IOException {
		
		debugLog("Receiving UpdatedChampionSelect data...");
		
		((ChampionSelectPage)currentPage).setChampion(inDataStream.readShort());
	}
	
	public void createGamePage() {
		
		debugLog("Creating Client Game Page");
		
		remove(currentPage);
		currentPage = new ClientGame();
		add(currentPage);
		
		requestFocusInWindow();	
	}
	
	public static void writeString(String s) {
		
		short l = (short) s.length();
		
		try {
			outDataStream.writeShort(l);
			for(int i=0; i<l; i++) {
				outDataStream.writeChar(s.charAt(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String readString() {
		try {
			short l = inDataStream.readShort();
			String s = new String("");
			for(int i=0; i<l; i++) {
				s = s + inDataStream.readChar();;
			}
			return s;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void connectToServer(String serverIp, int p, String name) {
		
		ServerIp = serverIp;
		port = p;
		UserName = name;
		
		Client.debugLog("Connecting to server...");
		
		try {
			
			connection = new Socket(ServerIp,port);		//Tries to connect to the server
			
			inStream = connection.getInputStream();
			inDataStream = new DataInputStream(inStream);
			
			outStream = connection.getOutputStream();
			outDataStream = new DataOutputStream(outStream);
			
			writeString(UserName);
			
			switch( inDataStream.read() ) {		//Wait for the server to know if he successfully logged
			case 1:
				
				Status = "MainPage";
				
				connected = true;
				
				Client.debugLog(" Connected!");
				
				inputThread = new Thread(new InputThread(), "Server input Thread");
				inputThread.start();
				
				setSize(600,425);
				setLocation(Math.max(0,getX()-150),Math.max(0,getY()-27));
				createMainPage();
				break;
			case 2:
				((LogInPage)currentPage).setErrorMessage("Name already used");
				Client.debugLog("Error! Name already used");
				break;
			case 3:
				((LogInPage)currentPage).setErrorMessage("Server is full");
				Client.debugLog("Error! Server is full");
				break;
			}
			
		} catch(IOException e ) {
			Client.debugLog("Error! Can't reach the server!");
			((LogInPage)currentPage).setErrorMessage("Can't reach the server!");
		}
	}
	
	public static void msgToServer(String s) {
		
		try {
			
			switch(s) {
			
			case "CreateLobby":
				outDataStream.write(1);
				break;

			case "ExitLobby":
				outDataStream.write(2);
				break;
			
			case "InviteToLobby":
				outDataStream.write(3);
				break;
				
			case "InviteAccepted":
				outDataStream.write(4);
				break;
				
			case "ChampionSelect":
				outDataStream.write(5);
				break;
			
			case "ChampionSelected":
				outDataStream.write(6);
				break;
				
			case "KeyData":
				outDataStream.write(7);
				break;
				
			case "SwitchTeam":
				outDataStream.write(8);
				break;
			default: break;
			
			}
		
		} catch (IOException e) {
			Client.debugLog("Connection Error");
		}
	}
	
	public static synchronized void sendToServer(ByteArrayOutputStream packet) {
		
		synchronized(outDataStream) {
			
			if(sending)
				try {
					outDataStream.wait();
				} catch (InterruptedException e1) {
				}
			
			sending = true;
			
			try {	
				outDataStream.write( packet.toByteArray() );
			} catch (IOException e) {
			}
			
			sending = false;
			outDataStream.notify();
		}

	}
	
	public void createInvitePanel(String inviter) {
	
		Client.invitePanel = new InvitePanel(inviter);
		currentPage.invitePanel = Client.invitePanel;
		currentPage.add(currentPage.invitePanel);
		currentPage.setComponentZOrder(invitePanel, 0);		//Sets the invite panel as the last thing to be drawn
	}
	
	private class TAdapter extends KeyAdapter {
       	
		public void keyPressed(KeyEvent e) {
			switch(e.getKeyCode()){
			case KeyEvent.VK_W:
				ClientGame.upKey = true;
				break;
			case KeyEvent.VK_S:
				ClientGame.downKey = true;
				break;
			case KeyEvent.VK_A:
				ClientGame.leftKey = true;
				break;
			case KeyEvent.VK_D:
				ClientGame.rightKey = true;
				break;
			case KeyEvent.VK_J:
				ClientGame.JKey = true;
				break;
			case KeyEvent.VK_K:
				ClientGame.KKey = true;
				break;
			case KeyEvent.VK_L:
				ClientGame.LKey = true;
				break;
			}
		}
    	
		public void keyReleased(KeyEvent e) {
    		switch(e.getKeyCode()){
			case KeyEvent.VK_W:
				ClientGame.upKey = false;
				break;
			case KeyEvent.VK_S:
				ClientGame.downKey = false;
				break;
			case KeyEvent.VK_A:
				ClientGame.leftKey = false;
				break;
			case KeyEvent.VK_D:
				ClientGame.rightKey = false;
				break;
			case KeyEvent.VK_J:
				ClientGame.JKey = false;
				break;
			case KeyEvent.VK_K:
				ClientGame.KKey = false;
				break;
			case KeyEvent.VK_L:
				ClientGame.LKey = false;
				break;
			}
        }
	}
	
	private class InputThread implements Runnable {

		@Override
		public void run() {
			
			while(connected) {
				
				try {
					
					switch( Client.inDataStream.read() ) {
					case 4:		//New invite received
						createInvitePanel(Client.readString());
						break;
					case 5:		//Lobby Exception
						if(Status == "LobbyPage") {
							((LobbyPage)currentPage).setInviteError("! - " + Client.readString() +  " - !");
						}
						break;
					case 6:		//Create Game Data
						Status = "Game";
						createGamePage();
						break;
					case 7:		//CreatedChampionSelectData
						Status = "ChampionSelect";
						createChampionSelectPage();
						break;
					case 8:		//Lobby data
						Status = "LobbyPage";
						createLobbyPage();
						break;
					case 9:		//Exit from lobby
						Status = "MainPage";
						createMainPage();
						break;
					case 10:	//Updated Champion Select Data
						if(Status == "ChampionSelect") updateChampionSelect();
						else {
							debugLog("Received champion select data while Status = " + Status);
						}
						break;
					case 11:	//Game Data
						((ClientGame)currentPage).readUpdateData();
						break;
					
					default: break;
					
					}
				
				} catch (IOException e) {
					Client.debugLog("Connection Error! Disconnected");
					connected = false;
				}
			}	
		}
	}
	
	public static String getPlayerSprite(short sprite, short frame) {
		return "sprites/Champions/Player" + sprite + "/Player" + sprite +"_" + frame +".png";
	}
	
	public static void debugLog(String s) {
		if(debug) System.out.println(s);
	}
	
	public static void main(String[] args) {
		new Client();
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent e) {
		
		if( ((ClientGame)currentPage) != null ) {
			
			((ClientGame)currentPage).drawing = false;
			((ClientGame)currentPage).notify();
		
		}
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {}
	
}