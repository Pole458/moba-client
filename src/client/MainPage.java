package client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import menu.ClientPage;
import menu.MyButton;

@SuppressWarnings("serial")
public class MainPage extends ClientPage {

	private MyButton PlayButton;
	
	public MainPage (ClientPage cp) {
		
		super();
		
		PlayButton = new MyButton("PLAY",new Color(0,154,215),new Rectangle(250,25,100,25));
	}
	
	public void paint(Graphics g) {
		
		setBounds(0,25,600,400);
		
		//g.setFont(new Font("Consolas",Font.PLAIN,14));
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		if(PlayButton!=null) PlayButton.draw(g);
		
		paintComponents(g);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(PlayButton.getSize().contains(e.getX(),e.getY()) ) {
			Client.msgToServer("CreateLobby");		//Tells the server to create a new lobby
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(PlayButton.getSize().contains(e.getX(),e.getY()) ) {
			if(!PlayButton.isSelected()) PlayButton.setSelected(true);
		} else {
			if(PlayButton.isSelected())	PlayButton.setSelected(false);
		}
	}

}
